# Orcid
[![LICENSE](https://img.shields.io/badge/license-GPLv2-blue.svg?style=flat-square)](./LICENSE)

## Introduction

ORCID integration, allowing users to login with ORCID credentials
## Maintainers

Current maintainers:

* [Alan Stanley](https://github.com/ajstanley)

## Usage

Navigate to admin/config/services/orcid to add your 
Client ID and Client secret (both will be supplied to 
you by [ORCID](https://orcid.org/my-orcid)).
When these two values have been saved users will be given
the option of signing in with ORCID credentials on the home page


## License

[GPLv2](http://www.gnu.org/licenses/gpl-2.0.txt)

